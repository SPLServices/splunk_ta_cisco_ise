Splunk Add-on for Cisco ISE version 2.1.2
Copyright (C) 2005-2015 Splunk Inc. All Rights Reserved.
 
The Splunk Add-on for Cisco ISE allows a Splunk Enterprise administrator to collect Cisco Identity Service Engine (ISE) syslog data. You can use Splunk Enterprise to analyze these logs directly or use them as a contextual data source to correlate with other communication and authentication data in Splunk Enterprise. This add-on provides the inputs and CIM-compatible knowledge to use with other Splunk Enterprise apps, such as Splunk App for Enterprise Security and Splunk App for PCI Compliance.

Documentation for this add-on is located at: http://docs.splunk.com/Documentation/AddOns/latest/CiscoISE/About

For installation and set-up instructions, refer to the Installation and Configuration section: 
http://docs.splunk.com/Documentation/AddOns/latest/CiscoISE/Hardwareandsoftwarerequirements

For release notes, refer to the Release notes section: http://docs.splunk.com/Documentation/AddOns/latest/CiscoISE/Releasenotes