ise.version = <string>
* Track the version of ISE target to ensure correct URL is configured for link.uri

ise.host = <string>
* Name the host of the ISE target to ensure correct URL is configured for link.uri
